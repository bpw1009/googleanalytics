
library(dplyr)
library(datasets)
library(shiny)
library(plotly)

# Define UI ----
ui <- fluidPage(
  sidebarLayout(
    sidebarPanel(
      tabsetPanel(
        tabPanel("Channel Group Filter",
                 checkboxGroupInput("rb", "Select Channel Group", choices = unique(revOnly$channelGrouping), selected = unique(revOnly$channelGrouping)[1],inline = FALSE),
                 selectInput("si","Select Country",choices = unique(revOnly$country)),
                 sliderInput("sl","transactionRevenue", min = 1, max = +23130, value = median(range(0,23130)))
        )
        
      )
      
    ),
    mainPanel(
      tabsetPanel(
        tabPanel("Histogram",
                 h2("Frequencies of Transactions by Revenue Amount"),
                 plotlyOutput("hist")
        ),
        tabPanel("Scatter 1",
                 h2("Scatter Plot 1"),
                 plotlyOutput("scatter1")
        ),
        tabPanel("Bar",
                 h2("Bar Plot"),
                 plotlyOutput("bar")
        ),
        tabPanel("Scatter 2",
                 h2("Scatter Plot 2"),
                 plotlyOutput("scatter2")
        ),
        tabPanel("Read Me",
                 h2("Read Me: Google Analytics Dataset"),
                 p("This project deals with a dataset from a Google Merchandise Store (also known as GStore)."),
                 p("The Sidebar Panel on the left has checkboxes for the different channel groups, a dropdown for the different coutnries, and a slider that serves to right censor the transaction revenue."),
                 p("There weren't many records containing revenue information countries besides the US so the dropdown has limited utility."),
                 h4("Insight 1: Histogram"),
                 p("Move the slider to around 200."),
                 p("Although there is some variation across channels and the distributions are right skewed, the center of the distributions seem to gravitate below $50."),
                 h4("Insight 2: Scatter Plot 1 - Revenue vs Pageviews"),
                 p("Across all channels, there doesn't seem to be a relationship between pageviews and revenue, although some channels (most likely due to the nature of the medium itself) seem to have fewer pageviews."),
                 p("Most records have fewer than 100 pageviews."),
                 h4("Insight 3: Bar Plot - Revenue by Operating System"),
                 p("In a digital marketing class, the professor referenced studies saying there is evidence Apple users tend to spend more. I wanted to see if this held true with the Google data."),
                 p("The bar plot shows the mean transaction revenue by operating system. Macintosh has the highest by far but iOS seems to lag behind."),
                 h4("Scatter 2: Digging Deeper"),
                 p("To get a closer look at how the operating system users are broken down, I \"hacked\" a scatter plot, leaving the OS on the X axis."),
                 p("Move the revenue slider left and hover over the data points in order to explore the tightly packed lower ranges.")
                 
                 
        #          
        # ),
       

      )
      
    )
  )
  
  
)
)
# Define server logic ----
server <- function(input, output, session) {

    filtered_data <- reactive({filter(revOnly, channelGrouping == input$rb)})
    observe({
      updateSelectInput(session,"si", choices = unique(filtered_data()$country))
    })
    
    
    filtered_data_2 <- reactive({filter(filtered_data(), country == input$si, transactionRevenue <= input$sl)})
    
    # filtered_data_3 <- reactive({filter(filtered_data(), country == input$si, transactionRevenue <= input$sl,  date>=input$dr[1], date <= input$dr[2])})
    
    output$hist <- renderPlotly(
      {
        p <- ggplot(filtered_data_2(), aes(x =transactionRevenue))+geom_histogram()+
          theme_bw()
        ggplotly(p)    
      }
    )
    output$scatter1 <- renderPlotly(
      {
        p <- ggplot(filtered_data_2(), aes(x=pageviews, y=transactionRevenue, color="channelGrouping"))+geom_point()
        theme_classic()
        ggplotly(p)
      }
    )
    output$bar <- renderPlotly(
      {
        p <- ggplot(filtered_data_2(), aes(x=operatingSystem, y=mean(transactionRevenue), color="channelGrouping"))+geom_bar(stat = "identity")
        theme_classic()
        ggplotly(p)
      }
    )
    output$scatter2 <- renderPlotly(
      {
        p <- ggplot(filtered_data_2(), aes(x=operatingSystem, y=transactionRevenue, color="channelGrouping"))+geom_point()
        theme_classic()
        ggplotly(p)
      }
    )
}

# Run the app ----
shinyApp(ui=ui, server=server)